<?php
require 'Psr4AutoloaderClass.php';
// 实例化加载器
$loader = new \Example\Psr4AutoloaderClass;

// 注册加载器
$loader->register();
// 为命名空间前缀注册基本路径
$loader->addNamespace('Foo\Bar', __DIR__ . '/packages/foo-bar/src');
$loader->addNamespace('Foo\Bar\Qux', __DIR__ . '/packages/foo-bar/src');
$loader->addNamespace('Foo\Bar', __DIR__ . '/packages/foo-bar/tests');

new \Foo\Bar\Qux\Quux;
new \Foo\Bar\Baz;